function Butterfly(){
    var tButterfly = new Sprite(scene, "bt.png", 80, 80);
    //properties
    tButterfly.setSpeed(0);
    tButterfly.setAngle(0);
    tButterfly.setPosition(320, 200);

    //methods
    tButterfly.motionButterfly = function()
    {
        if (keysDown[K_W] || keysDown[K_UP]){
            if(this.speed < 10){
                this.changeSpeedBy(1);
            }
        }
        if (keysDown[K_S]  || keysDown[K_DOWN]){
            if(this.speed > -3){
                this.changeSpeedBy(-1);
            }
        }
        if (keysDown[K_A]  || keysDown[K_LEFT]){
            this.changeAngleBy(-5);
        }
        if (keysDown[K_D]  || keysDown[K_RIGHT]){
            this.changeAngleBy(5);
        }
        
    }

    return tButterfly;
}
function Flower(){
    var tFlower = new Sprite(scene, "flower.png", 50, 50);
    tFlower.setSpeed(0);
    tFlower.setPosition(Math.random() * this.cWidth, Math.random() * this.cHeight);

    tFlower.motionFlower = function(){
        newDir = (Math.random()*90)-45;
        tFlower.changeAngleBy(newDir);
    }

    tFlower.respawn = function(){
        newX = Math.random() * this.cWidth;
        newY = Math.random() * this.cHeight;
        this.setPosition(newX, newY);
    }

    return tFlower;
}

function Bee(){
    var tBee = new Sprite(scene, "beee.png", 60, 60);
    tBee.setSpeed(5);
    tBee.setPosition(Math.random() * this.cWidth, Math.random() * this.cHeight);

    tBee.motionBee = function(){
        newDir = (Math.random()*90)-45;
        tBee.changeAngleBy(newDir);
    }

    tBee.respawn = function(){
        newX = Math.random() * this.cWidth;
        newY = Math.random() * this.cHeight;
        this.setPosition(newX, newY);
    }

    return tBee;
}
